#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char const *dogGreet(void) {
    return "vau!";
}

char const *dogMenu(void) {
    return "kuhanu govedinu";
}

char const *catGreet(void) {
    return "mijau!";
}

char const *catMenu(void) {
    return "konzerviranu tunjevinu";
}

typedef struct Animal {
    char *name;

    const char *(**fun_table)(void);
} Animal;

char const *(*dog_fun_table[2])(void) = {dogGreet, dogMenu};

char const *(*cat_fun_table[2])(void) = {catGreet, catMenu};

Animal *createDog(char *name) {
    Animal *dog = (Animal *) malloc(sizeof(Animal));
    dog->name = name;
    dog->fun_table = dog_fun_table;
    return dog;
}

Animal *createCat(char *name) {
    Animal *cat = (Animal *) malloc(sizeof(Animal));
    cat->name = name;
    cat->fun_table = cat_fun_table;
    return cat;
}

void animalPrintGreeting(Animal *animal) {
    printf("%s\n", (char *) animal->fun_table[0]());
}

void animalPrintMenu(Animal *animal) {
    printf("%s\n", animal->fun_table[1]());
}

Animal initAnimal(char *name, const char *(**fun_table)(void), Animal *animal) {
    strcpy((*animal).name, name);
    animal->fun_table = fun_table;
    return *animal;
}

Animal createDogStack(char *name) {
    Animal dog;
    return initAnimal(name, dog_fun_table, &dog);
}

Animal createCatStack(char *name) {
    Animal cat;
    return initAnimal(name, cat_fun_table, &cat);
}

// Why doesn't this work???
Animal **createMultipleDogs(int count) {
    int i;
    Animal **dogs = (Animal **) malloc(sizeof(Animal *) * count);

    for (i = 0; i < count; i++) {
        char dog_name[6];
        char dog_id[2]; // only for this example, this has to be calculated

        sprintf(dog_id, "%d", i);
        strcat(dog_name, "Dog_");
        strcat(dog_name, dog_id);
        dogs[i] = createDog(dog_name); // Zasto se ovo polomi pobogu???
    }

    return dogs;
}

void freeAnimals(Animal **dogs, int num_of_dogs) {
    int i;
    for (i = 0; i < num_of_dogs; i++) {
        free(dogs[i]);
    }
    free(dogs);
}


void testAnimals(void) {
    int i;

    Animal *p1 = createDog("Hamlet");
    Animal *p2 = createCat("Ofelija");
    Animal *p3 = createDog("Polonije");

    animalPrintGreeting(p1);
    animalPrintGreeting(p2);
    animalPrintGreeting(p3);

    animalPrintMenu(p1);
    animalPrintMenu(p2);
    animalPrintMenu(p3);

    free(p1);
    free(p2);
    free(p3);

    Animal **dogs = createMultipleDogs(10);
    for (i = 0; i < 10; i++) {
        printf("%s\n", dogs[i]->name);
    }

    freeAnimals(dogs, 10);
}

int main(int argc, char *argv[]) {
    testAnimals();
    Animal dogHamlet = createDogStack("Hamlet");
    Animal dogPolonije = createDogStack("Polonije");
    Animal catOfelija = createCatStack("Ofelija");

    printf("%s\n", dogHamlet.name);
    printf("%s\n", dogPolonije.name);
    printf("%s\n", catOfelija.name);

    return 0;
};